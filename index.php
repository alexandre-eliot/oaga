<!DOCTYPE html>
<html>
<head>
	<title>OAGA</title>
	<link rel="icon" href="imgs/icon.ico">
	<link rel="stylesheet" type="text/css" href="styles/style.css">
</head>
<body>
	<header>
		<div id="oaga">
			<figure><img src="imgs/icon.png" title="Oaga"></figure>
		</div>
		<input type="checkbox" id="burger" name="burger">
		<label for="burger">
			<span></span>
			<span></span>
			<span></span>
		</label>
		<nav>
			<ul id="menu">
				<li><a href="#first">Home</a></li>
				<li><a href="#concept">Concept</a></li>
				<li><a href="#photos">Photos</a></li>
				<li><a href="#ambitions">Ambitions</a></li>
				<li><a href="#team">Equipe</a></li>
				<li><a href="#about">A propos</a></li>
				<li><a href="#contact">Contact</a></li>
			</ul>
		</nav>
	</header>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script>
		// $('#burger').click(menuWhite());
		// $(document).ready(menuWhite());
		// function menuWhite(){
		// 	if($('#burger').attr('checked')!=undefined){
		// 		$('#header').addClass('scrolled');
		// 	} else {
		// 		$('#header').removeClass('scrolled');
		// 	}
		// }
		// make the header change color whilst scrolling
		$(window).scroll(function(){
			var fromTopPx = 300; // distance to trigger
			var scrolledFromtop = $(window).scrollTop();
			if(scrolledFromtop > fromTopPx){
				$('header').addClass('scrolled');
			}else{
				$('header').removeClass('scrolled');
			}
		});

		// make the animation of going to #id smooth
		$(document).on('click', 'a[href^="#"]', function (event) {
			event.preventDefault();

			$('html, body').animate({
				scrollTop: $($.attr(this, 'href')).offset().top
			}, 500);
		});

		$(document).ready(function() {
			// make the items appear whilst scrolling
			showItems();
			// make clicking elsewhere of the menu close it
			quitMenu();
			// Every time the window is scrolled
			$(window).scroll( function(){
				/* Check the location of each desired element */
				showItems(); 
			});
			function showItems(){	
				$('.hidden').each( function(i){
					var middle_of_object = $(this).offset().top + $(this).outerHeight();
					var bottom_of_window = $(window).scrollTop() + $(window).height();
					/* If the object is completely visible in the window, fade it it */
					if( bottom_of_window > middle_of_object ){
						$(this).animate({'opacity':'1'},500);
					}
				}); 
			}
			function quitMenu(){
				$(document).mouseup(function (e){
					var container = $('header nav ul');
					// if the target of the click isn't the container nor a descendant of the container
					if (!container.is(e.target) && container.has(e.target).length === 0){
						$('#burger').attr('checked',false);
						$('#burger').prop('checked',false);
					}
				});
			}
		});
	</script>
	<main>
		<section>
			<article id="first">
				<div class="content">
					<h2>Concrete speakers ! Better sound, better quality !</h2>
					<h1>Oaga</h1>
				</div>
			</article>
			<article id="concept">
				<div class="content">
					<h1 class="hidden">Concept</h1>
					<p class="hidden">Le produit final est une enceinte bluetooth en béton, esthétique, qui se branche sur le secteur et qui s'utilise avec un seul bouton. Elle a deux enceintes qui partagent une caisse du volume approprié pour produire un son qui couvre le spectre 20Hz-20KHz d'une fidélité supérieure à 90 décibel. Ce produit présente de multiples avantages sur les alternatives proposées par JBL et Bose.</p>
				</div>
			</article>
			<article id="photos">
				<div class="content">
					<h1 class="hidden">Photos</h1>
					<div id="pictures">
						<figure><img src="imgs/front.png"></figure>
						<figure><img src="imgs/top.png"></figure>
					</div>
				</div>
			</article>
			<article id="ambitions">
				<div class="content">
					<h1 class="hidden">Ambitions</h1>
					<p class="hidden">Cette enceinte coûte moins de 75€ à fabriquer et moins de 50€ à produire en masse donc elle peut être vendue à moins de 150€ tout en étant très rentable et convenant aux personnes qui aiment le design, le prix attirant et la qualité proposée.</p>
				</div>
			</article>
			<article id="team">
				<div class="content">
					<h1 class="hidden">Equipe</h1>
					<div id="team">
						<div class="hidden person">
							<img src="imgs/oliver.png">
							<h2>Oliver LLOYD</h2>
							<p>Instigateur du projet</p>
						</div>
						<div class="hidden person">
							<img src="imgs/alexis.png">
							<h2>Alexis HAMACHE GILON</h2>
							<p>Agent d'entretien</p>
						</div>
						<div class="hidden person">
							<img src="imgs/guillaume.png">
							<h2>Guillaume SICART</h2>
							<p>Economiste</p>
						</div>
						<div class="hidden person">
							<img src="imgs/alexandre.png">
							<h2>Alexandre ELIOT</h2>
							<p>Webmaster</p>
						</div>
					</div>
				</div>
			</article>
			<article id="about">
				<div class="content">
					<h1 class="hidden">A propos</h1>
					<p class="hidden">Primi igitur omnium statuuntur Epigonus et Eusebius ob nominum gentilitatem oppressi. praediximus enim Montium sub ipso vivendi termino his vocabulis appellatos fabricarum culpasse tribunos ut adminicula futurae molitioni pollicitos.</p>
				</div>
			</article>
			<article id="contact">
				<div class="content">
					<h1 class="hidden">Contact</h1>
					<?php include("formHandler.php");?>
					<form action="" method="POST">
						<div>
							<div class="hidden">
								<label for="prenom">Prénom</label>
								<input id="prenom" type="text" name="prenom" placeholder="Prénom" required=""><br>
							</div>
							<div class="hidden">
								<label for="nom">Nom</label>
								<input id="nom" type="text" name="nom" placeholder="Nom" required=""><br>
							</div>
						</div>
						<div class="hidden">
							<label for="email">Email</label>
							<input id="email" type="email" name="email" placeholder="E-mail" required=""><br>
						</div>
						<div class="hidden">
							<label for="sujet">Sujet</label>
							<input id="sujet" type="text" name="sujet" placeholder="Sujet" required=""><br>
						</div>
						<div class="hidden">
							<label for="message">message</label>
							<textarea id="message" name="message" rows="4" placeholder="Ecrivez votre message ici.." required=""></textarea><br>
						</div>
						<input type="submit" value="Envoyer">
					</form>
					<?php
					if($submitted) {
						$h1 = '<h1>';
						if($sent) $h1.='Email sent successfully !';
						else $h1.='An error has occured, the email has not been sent.';
						$h1.='</h1>';
						echo $h1;
					}
					?>
				</div>
			</article>
		</section>
		<footer>
			<span>Copyright © <?=date("Y");?> Oaga. All rights reserved</span>
		</footer>
	</main>
</body>
</html>