<?php
	// '/[a-zA-Z\s]+/'; // alpha or spaces, at least 1 char
	define('EMAIL', 'your@email.com');
	$dfnPrenom = isset($_POST['prenom']);
	$dfnNom = isset($_POST['nom']);
	$dfnSujet = isset($_POST['sujet']);
	$dfnMessage = isset($_POST['message']);
	$dfnEmail = isset($_POST['email']);
	$message = '';
	$sent = false;
	$submitted=false;

	$passage_ligne="\n";
	if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", EMAIL)){
		$passage_ligne = "\r\n";
	} else {
		$passage_ligne = "\n";
	}


	if( $dfnPrenom and $dfnNom and $dfnSujet and $dfnMessage and $dfnEmail ){

		$prenom = htmlspecialchars($_POST['prenom']);
		$nom = htmlspecialchars($_POST['nom']);
		$email=htmlspecialchars($_POST['email']);
		$sujet=htmlspecialchars($_POST['sujet']);
		$message=htmlspecialchars($_POST['message']);

		$submitted=true;

		$vldPrenom = filter_var($prenom,
			FILTER_VALIDATE_REGEXP,
			array('options' => array("regexp"=>'/[a-zA-Z\s]{1,35}/') ) 
		);
		$vldNom = filter_var($nom,
			FILTER_VALIDATE_REGEXP,
			array('options' => array("regexp"=>'/[a-zA-Z\s]{1,35}/') )
		);
		$vldSujet = filter_var($sujet,
			FILTER_VALIDATE_REGEXP,
			array('options' => array("regexp"=>'/[a-zA-Z\s]/') )
		);
		$vldMessage = filter_var($message,
			FILTER_VALIDATE_REGEXP,
			array('options' => array("regexp"=>'/.{10,}/') )
		);
		$vldEmail = filter_var($email,FILTER_VALIDATE_EMAIL);

		if( $vldPrenom and $vldNom and $vldSujet and $vldMessage and $vldEmail ){

			$encoding = "utf-8";

   			// Preferences for Subject field
			$subject_preferences = array(
				"input-charset" => $encoding,
				"output-charset" => $encoding,
				"line-length" => 76,
				"line-break-chars" => "\r\n"
			);

  			// Mail header
			$header = "Content-type: text/html; charset=".$encoding." \r\n";
			$header .= "From: ".$prenom." ".$nom." <".$email."> \r\n";
			$header .= "MIME-Version: 1.0 \r\n";
			$header .= "Content-Transfer-Encoding: 8bit \r\n";
			$header .= "Date: ".date("r (T)")." \r\n";
			$header .= 'X-Mailer: PHP/' . phpversion();
			$header .= iconv_mime_encode("Subject", $sujet, $subject_preferences);

			$message = message($prenom, $nom, $message, $passage_ligne);

			$sent = mail(EMAIL, $sujet, $message, $header );
			if(!$sent)
				$sent = mail(EMAIL, $sujet, $message );

		}
	}

	function message($prenom, $nom, $message, $passage_ligne){
		$content='Message de '.$prenom.' '.$nom.$passage_ligne.$passage_ligne;
		$message.=multireplace(array("\n","\r\n","\r"), $message,$passage_ligne);
		$content.=$message;
		return $content;
	}

	function multireplace ($delimiters,$string,$replacement) {
		for ($i=0; $i < count($delimiters); $i++) {
			$ready = str_replace($delimiters[$i], $delimiters[0], $string);
		}
		$launch = str_replace($delimiters[0], $replacement, $ready);
		return $launch;
	}
	?>